


"""Defines URL patterns for users"""

from django.conf.urls import url
from . import views

from django.contrib.auth import views as auth_views
app_name='users'


urlpatterns = [
              # Login page


url('login/', auth_views.LoginView.as_view(), name='login'),
# Logout page
url('logout/', views.logout_view, name='logout'),
url('register/', views.register, name='register'),

]

