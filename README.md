# Django Learning Log

A Evernote like app where each user can store daily writings in an secure non sharable account

## Getting Started

    code
$ Git clone https://gitlab.com/officerebel/django_learning_log.git


### Prerequisites

What things you need to install the software and how to install them

```
make a virtual env with
$pipenv shell


```

### Installing




### Install the requirements

```

$ pip install -r requirements.txt
```

### Make loginname and password for superuser

```
$ python manage.py createsuperuser 
```

### Run the project
```
$ python manage.py runserver
```

### Open the browser
```
open localhost:8000
```

### And coding style tests

No test were made
```
Give an example
```

## Deployment

Deploy to heroku

## Built With

* [Django](https://www.djangoproject.com) - The web framework used


## Authors

* **Tim Vogt** - *Initial work* - [Officerebel](https://gitlab.com/officerebel)



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

